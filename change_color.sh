#!/bin/bash

echo "This script will allow you to chang the accent color of this Ulauncher Arc-based Theme (default color is red)."
echo "Please note that this script will only change themes, that are in the same folder as this script file. This is usually the case when you just downloaded it from github, but if you already moved the theme folders into ~/.config/ulauncher/user-themes this script will error."
echo "Please input the new accent color you want the theme to have (in 6-digit hex):"
read -p "New Color: " new_color

#remove # if the color code starts with it
if [[ $new_color == \#* ]];
then
	new_color="${new_color:1}"
fi

#turn all letters lowercase
new_color=${new_color,,}

#color code is not 6 digits long
if [ ${#new_color} != 6 ];
then
	echo "Your entered color is not a valid 6-digit hexcode for colors"
	exit 1
fi

#color code is not valid hex code
if ! [[ "$new_color" =~ [0-9a-f] ]];
then
	echo "Your entered color is not a valid hexcode"
	exit 1
fi

echo "Changing accent color to #$new_color"

#change accent color declaration in dark and light theme
for x in Arc-*-Dark/theme.css;
do
	sed -i "s/arc_accent #.*;/arc_accent #$new_color;/g" "$x"
done

for x in Arc-*-Dark/manifest.json;
do
	sed -i "s/\"when_not_selected\": \"#.*\"/\"when_not_selected\": \"#$new_color\"/g" "$x"
done

echo "Changed accent color in dark theme"

for x in Arc-*-Light/theme.css;
do
	sed -i "s/arc_accent #.*;/arc_accent #$new_color;/g" "$x"
done

echo "Changed accent color in light theme"

for x in Arc-*-Light/manifest.json;
do
	sed -i "s/\"when_not_selected\": \"#.*\"/\"when_not_selected\": \"#$new_color\"/g" "$x"
done

echo "Now assign a name, under which this theme will be displayed in the Menu (Arc [NAME] Dark / Arc [NAME] Light)"
read -p "New name: " new_name

#rename folders
for x in Arc-*-Dark;
do
	mv "$x" "Arc-$new_name-Dark"
done

for x in Arc-*-Light;
do
	mv "$x" "Arc-$new_name-Light"
done

#change proper name in manifests
for x in Arc-*-Dark/manifest.json;
do
	sed -i "s/\"name\": \"arc-.*-dark\",/\"name\": \"arc-${new_name,,}-dark\",/g" "$x"
done

for x in Arc-*-Light/manifest.json;
do
	sed -i "s/\"name\": \"arc-.*-light\",/\"name\": \"arc-${new_name,,}-light\",/g" "$x"
done

#change display name in manifests
for x in Arc-*-Dark/manifest.json;
do
	sed -i "s/\"display_name\": \"Arc .* Dark\",/\"display_name\": \"Arc $new_name Dark\",/g" "$x"
done

for x in Arc-*-Light/manifest.json;
do
	sed -i "s/\"display_name\": \"Arc .* Light\",/\"display_name\": \"Arc $new_name Light\",/g" "$x"
done

echo "Finished changing the colors, you can copy Arc-$new_name-Light and Arc-$new_name-Dark to your ~/.config/ulauncher/user-themes directory now"
