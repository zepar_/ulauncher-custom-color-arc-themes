# Ulauncher Custom Color Arc Themes

Themes for Ulauncher (https://github.com/Ulauncher/Ulauncher) based on a colored Arc Theme (https://github.com/erikdubois/Arc-Theme-Colora)

Default color is red (#e25252), but can be changed with the change_color.sh script file  
(requires at current version of `bash`, or other shell with support for `[[`; and `sed`, both usually already installed on your system)

![](https://i.imgur.com/hBhkoB1.png)

![](https://i.imgur.com/hxlC1YA.png)
